<?php
    $content = file_get_contents(__DIR__ . '/notebook.json');
    $result = json_decode($content, true);
    // echo "<pre>";
    // print_r($result);

?>

<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <title>Notebook</title>
  <style type="text/css">
      td:nth-child(1) {
        text-align: center;                   
      }
      td, th {
        border: 2px solid;  
        padding: 10px;          
      }
      table {
        border-collapse: collapse; 
      }
  </style>
</head>
<body>
    <table>
        <tr>
            <th>Номер по порядку</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Город</th>
            <th>Улица</th>
            <th>Дом</th>
            <th>Индекс</th>
            <th>Телефон</th>
            <th>Дополнительный телефон</th>
        </tr>
        <?php $count = 1; ?>
        <?php foreach ($result as $entry) { ?>
            <tr>                 
                <td><?php echo $count; ?></td>                                
                <td><?php echo $entry['firstName']; ?></td>
                <td><?php echo $entry['lastName']; ?></td>
                <td><?php echo $entry['address']['city']; ?></td>
                <td><?php echo $entry['address']['street']; ?></td>
                <td><?php echo $entry['address']['house']; ?></td>
                <td><?php echo $entry['address']['postalCode']; ?></td>
                <td><?php echo $entry['phoneNumber'][0]; ?></td>
                <td><?php echo $entry['phoneNumber'][1]; ?></td>                
                <?php $count++; ?>                     
            </tr>
        <?php } ?>
    </table>
</body>
</html>